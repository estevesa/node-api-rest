const connection = require('../database/connection')
const express = require('express')
const router = express.Router()
const taskController = require('../controller/taskController')

router.post('/novaTarefa', taskController.novaTarefa)
router.get('/tarefas', taskController.listarTarefas)
router.get('/tarefa/:id', taskController.listarTarefa)
router.put('/atualizar/tarefa/:id', taskController.atualizarTarefa)
router.put('/atualizarjiraTask/:id', taskController.atualizarFlagJiraTaskImportada)
router.delete('/delete/tarefa/:id', taskController.removerTarefa)

module.exports = router
