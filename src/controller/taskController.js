const database = require('../database/connection')

class taskController {
    novaTarefa(request, response) {
        const {ticketNo, startDate, timeSpent, coment, jiraAddTask, dateTime} = request.body
        console.log(ticketNo, startDate, timeSpent, coment, jiraAddTask, dateTime)
        database.insert({ticketNo, startDate, timeSpent, coment, jiraAddTask, dateTime}).table("tasks").then(data => {
            response.json({message: 'Tarefa criada com sucesso!'})
            console.log(data)
        }).catch (error => {
            console.log(error)
        })
    }
    listarTarefas(request, response) {
        database.select("*").table("tasks").then(tarefas => {
            response.json(tarefas)
        }).catch(error => {
            console.log(error)
        })
    }
    listarTarefa(request, response) {
        const id = request.params.id
        database.select("*").table("tasks").where({id:id}).then(tarefa => {
            response.json(tarefa)
        }).catch(error => {
            console.log(error)
        })
    }
    atualizarTarefa(request, response) {
        const id = request.params.id
        const {ticketNo, startDate, timeSpent, coment, jiraAddTasks, dateTime} = request.body
        database.where({id:id}).update({ticketNo:ticketNo,
                                        startDate:startDate,
                                        timeSpent:timeSpent,
                                        coment:coment,
                                        jiraAddTasks:jiraAddTasks,
                                        dateTime:dateTime}).table("tasks").then(data => {
        response.message = 'Tarefa atualizada com sucesso'
        response.json({message: response.message})
        }).catch(error => {
            response.message = error
            response.json(error)
        })
    }
    atualizarFlagJiraTaskImportada(request,response){
        const id = request.params.id
        const {jiraAddTask} = request.body
        database.where({id:id}).update({jiraAddTask:jiraAddTask}).table("tasks").then(data=>{
            response.json({message:"Atualização do flag realizada com sucesso"})
        }).catch(error=>{
            response.json(error)
        })
    }
    removerTarefa(request, response) {
        const id = request.params.id
        database.where({id:id}).del().table("tasks").then(data => {
            response.message = 'Tarefa excluída com sucesso!'
            response.json({message: response.message})
        }).catch(error => {
            response.message = error
            response.json(error)
        })
    }
}
module.exports = new taskController()
